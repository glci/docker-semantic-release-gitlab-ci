# Semantic Release Docker image for GitLab CI

A docker image to enforce Semantic Versioning with the help of [semantic-release](https://semantic-release.gitbook.io/semantic-release/).

## Usage

As the Docker image is automatically build on GitLab, you can use it right in your pipeline:

```yaml
stages:
  - release

publish:
  image: registry.gitlab.com/glci/docker-semantic-release-gitlab-ci:20231011
  stage: release
  script:
    - npx semantic-release --branches main
  only:
    - main
```
